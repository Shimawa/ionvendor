# accents
PRODUCT_PACKAGES += \
    Amber \
    Black \
    Blue \
    BlueGrey \
    Brown \
    CandyRed \
    Cyan \
    DeepOrange \
    DeepPurple \
    ExtendedGreen \
    Green \
    Grey \
    Indigo \
    JadeGreen \
    LightBlue \
    LightGreen \
    Lime \
    Orange \
    PaleBlue \
    PaleRed \
    Pink \
    Purple \
    Red \
    Teal \
    Yellow \
    White \
    UserOne \
    UserTwo \
    UserThree \
    UserFour \
    UserFive \
    UserSix \
    UserSeven \
    ObfusBleu \
    NotImpPurple \
    Holillusion \
    MoveMint \
    FootprintPurple \
    BubblegumPink \
    FrenchBleu \
    Stock \
    ManiaAmber \
    SeasideMint \
    DreamyPurple \
    SpookedPurple \
    HeirloomBleu \
    TruFilPink \
    WarmthOrange \
    ColdBleu \
    DiffDayGreen \
    DuskPurple \
    BurningRed \
    HazedPink \
    ColdYellow \
    NewHouseOrange \
    IllusionsPurple \
    AccentOnePlusRed \
    UserEight \
    UserNine

# black
PRODUCT_PACKAGES += \
    SystemBlack \
    SystemUIBlack \
    DocumentsUIBlack

# dark
PRODUCT_PACKAGES += \
    FaceLockDark \
    SystemDark \
    SystemUIDark \
    GBoardDark \
    WellbeingDark \
    DocumentsUIDark

# light
PRODUCT_PACKAGES += \
    GBoardLight

# overlay
PRODUCT_PACKAGES += \
    NoCutoutOverlay

# qs
PRODUCT_PACKAGES += \
    QStileDefault \
    QStileCircleTrim \
    QStileCircleDualTone \
    QStileCircleGradient \
    QStileCookie \
    QStileDottedCircle \
    QStileDualToneCircle \
    QStileInk \
    QStileInkdrop \
    QStileMountain \
    QStileNinja \
    QStileOreo \
    QStileOreoCircleTrim \
    QStileOreoSquircleTrim \
    QStilePokesign \
    QStileSquaremedo \
    QStileSquircle \
    QStileSquircleTrim \
    QStileTeardrop \
    QStileWavey

# qs header
PRODUCT_PACKAGES += \
    QSHeaderBlack \
    QSHeaderGrey \
    QSHeaderLightGrey \
    QSHeaderAccent \
    QSHeaderTransparent

# switch
PRODUCT_PACKAGES += \
    ionSwitch \
    SwitchMD2 \
    SwitchOnePlus \
    SwitchStock \
    SwitchOne \
    SwitchTwo \
    SwitchThree \
    SwitchFour \
    SwitchFive \
    SwitchSix \
    SwitchSeven \
    SwitchEight \
    SwitchNine
