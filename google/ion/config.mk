ifeq ($(TARGET_GAPPS_ARCH),)
$(error "GAPPS: TARGET_GAPPS_ARCH is undefined")
endif

ifneq ($(TARGET_GAPPS_ARCH),arm)
ifneq ($(TARGET_GAPPS_ARCH),arm64)
$(error "GAPPS: Only arm and arm64 are allowed")
endif
endif

TARGET_MINIMAL_APPS ?= false

$(call inherit-product, vendor/google/ion/common-blobs.mk)

# Include package overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += vendor/google/ion/overlay
DEVICE_PACKAGE_OVERLAYS += \
    vendor/google/ion/overlay/common/

# framework
PRODUCT_PACKAGES += \
    com.google.android.maps \
    com.google.android.media.effects \
    com.google.widevine.software.drm

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    com.google.android.dialer.support
endif

# app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    FaceLock \
    GoogleContactsSyncAdapter \
    GoogleExtShared \
    NexusWallpapersStubPrebuilt2018 \
    LatinIMEGooglePrebuilt \
    PrebuiltDeskClockGoogle

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    PrebuiltBugle
endif

ifeq ($(TARGET_MINIMAL_APPS),false)

ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
PRODUCT_PACKAGES += \
    arcore
endif

PRODUCT_PACKAGES += \
    CalendarGooglePrebuilt \
    Photos \
    Chrome \
    GoogleTTS \
    talkback
endif

# priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    AndroidPlatformServices \
    ConfigUpdater \
    ConnMetrics \
    GoogleBackupTransport \
    GoogleContacts \
    GoogleExtServices \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    Phonesky \
    PrebuiltGmsCorePi \
    SetupWizard \
    StorageManagerGoogle \
    Turbo \
    WallpaperPickerGooglePrebuilt \
    Lawnchair \
    Velvet

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    GoogleDialer
endif

