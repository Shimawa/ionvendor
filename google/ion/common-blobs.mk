ifeq ($(TARGET_MINIMAL_APPS),true)
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/google/ion/lib_old,system/lib)
else
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/google/ion/lib,system/lib)
endif
ifeq ($(TARGET_GAPPS_ARCH),arm64)
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/google/ion/lib64,system/lib64)
endif
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/google/ion/usr,system/usr)

PRODUCT_COPY_FILES += \
    vendor/google/ion/etc/default-permissions/default-permissions.xml:system/etc/default-permissions/default-permissions.xml \
    vendor/google/ion/etc/default-permissions/opengapps-permissions.xml:system/etc/default-permissions/opengapps-permissions.xml \
    vendor/google/ion/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
    vendor/google/ion/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml \
    vendor/google/ion/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml \
    vendor/google/ion/etc/permissions/privapp-permissions-google.xml:system/etc/permissions/privapp-permissions-google.xml \
    vendor/google/ion/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml \
    vendor/google/ion/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml \
    vendor/google/ion/etc/sysconfig/google-hiddenapi-package-whitelist.xml:system/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    vendor/google/ion/etc/preferred-apps/google.xml:system/etc/preferred-apps/google.xml

# Bootanimation
ifeq ($(TARGET_BOOT_ANIMATION_BLACK),true)
     PRODUCT_COPY_FILES += vendor/google/ion/media/bootanimation.zip:system/media/bootanimation.zip
else ifeq ($(TARGET_BOOT_ANIMATION_RES),720)
     PRODUCT_COPY_FILES += vendor/google/ion/media/bootanimation_720.zip:system/media/bootanimation.zip
else ifeq ($(TARGET_BOOT_ANIMATION_RES),1080)
     PRODUCT_COPY_FILES += vendor/google/ion/media/bootanimation_1080.zip:system/media/bootanimation.zip
else ifeq ($(TARGET_BOOT_ANIMATION_RES),1440)
     PRODUCT_COPY_FILES += vendor/google/ion/media/bootanimation_1440.zip:system/media/bootanimation.zip
else
     $(warning "TARGET_BOOT_ANIMATION_RES is undefined, assuming 1080p")
     PRODUCT_COPY_FILES += vendor/google/ion/media/bootanimation_1080.zip:system/media/bootanimation.zip
endif

# fonts
PRODUCT_COPY_FILES += \
    vendor/google/ion/fonts/GoogleSans-Regular.ttf:system/fonts/GoogleSans-Regular.ttf \
    vendor/google/ion/fonts/GoogleSans-Medium.ttf:system/fonts/GoogleSans-Medium.ttf \
    vendor/google/ion/fonts/GoogleSans-MediumItalic.ttf:system/fonts/GoogleSans-MediumItalic.ttf \
    vendor/google/ion/fonts/GoogleSans-Italic.ttf:system/fonts/GoogleSans-Italic.ttf \
    vendor/google/ion/fonts/GoogleSans-Bold.ttf:system/fonts/GoogleSans-Bold.ttf \
    vendor/google/ion/fonts/GoogleSans-BoldItalic.ttf:system/fonts/GoogleSans-BoldItalic.ttf

ifeq ($(IS_PHONE),true)
PRODUCT_COPY_FILES += \
    vendor/google/ion/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml
endif

